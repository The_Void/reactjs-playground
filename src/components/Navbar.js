import React from 'react';
// Imports for Router
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
// Importing components
import Home from './Home';
import About from './About';
import Contact from './Contact';
// Import Scss
import '../styles/Navbar.scss'

const Navbar = () => {
    return (
        <Router>
            <header className="navbar-wrapper">
                <nav className="navbar navbar-expand-lg navbar-light bg-light container">
                    <a className="navbar-brand"><Link to={'/'}>MVVT</Link></a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div className="navbar-nav">
                        <a className="nav-item nav-link"><Link to={'/'}>Home</Link></a>
                        <a className="nav-item nav-link"><Link to={'/about'}>About</Link></a>
                        <a className="nav-item nav-link"><Link to={'/contact'}>Contact</Link></a>
                        </div>
                    </div>
                </nav>
            </header>
            <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/about' component={About} />
                <Route exact path='/contact' component={Contact} />
            </Switch>
        </Router> 
    );
}

export default Navbar;