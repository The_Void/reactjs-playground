import React, {useEffect} from 'react';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import './styles/App.scss';

const chaos = () => {
	alert('Chaos initiated');
}

const App = () => {
  useEffect(() => {
    console.log('componentDidMount');
  }, []);
  return (
    <div className="App">
      <Navbar />
      <Footer />
    </div>
  );
}

export default App;
