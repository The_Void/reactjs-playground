import React from 'react';
import '../styles/App.scss';

const Footer = () => {
    return (
        <div className="footer-wrapper">
            <footer className="container">
            <p>Posted by: Hege Refsnes</p>
            <p>Contact information: 
                <a href="mailto:someone@example.com">
                    someone@example.com
                </a>.
            </p>    
            </footer>
        </div>
    );
}

export default Footer;